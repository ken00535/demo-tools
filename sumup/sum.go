package sumup

import "fmt"

// Sum returns the sum of the parameters
func Sum(i int, j int) int {
	return i + j
}

func Hello() {
	fmt.Println("hello")
}

func Hello2() {
	fmt.Println("hello")
}

func Hello3() {
	fmt.Println("hello")
}
