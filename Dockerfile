# Base image: https://hub.docker.com/_/golang/
FROM golang:1.18

# Install golint
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH
